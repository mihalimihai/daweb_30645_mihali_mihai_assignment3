-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2020 at 07:38 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment3`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('01e32389710e8a110e01aaefb90f96d39bbbd3badfaa051f223dd686d42dccd517dfd92b6c8eda7f', 26, 1, 'mihalimihai@gmail.com-2020-05-05 18:09:54', '[]', 0, '2020-05-05 15:09:54', '2020-05-05 15:09:54', '2021-05-05 18:09:54'),
('02f0fc45ee492d8348c49ce87fb141a27b9eaed8fcba5d792c830a19ac553c6e1e7217d3663e6154', 33, 1, 'mihalimihai97@gmail.com-2020-05-06 17:13:54', '[]', 0, '2020-05-06 14:13:54', '2020-05-06 14:13:54', '2021-05-06 17:13:54'),
('06e8dd449c9ffce5f168eb7380fe0ec912dc1ae1effbb17092feb370c62f9a847b84a7c90a6b6c02', 26, 1, 'mihalimihai@gmail.com-2020-05-06 13:34:57', '[]', 0, '2020-05-06 10:34:58', '2020-05-06 10:34:58', '2021-05-06 13:34:58'),
('07172b765d785e0298f16d2e1cf0011ec3f951dee2af06392b43c0c623d435ef6a5736f1e079eeeb', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:41:40', '[]', 0, '2020-05-05 11:41:40', '2020-05-05 11:41:40', '2021-05-05 14:41:40'),
('0cd5c9a456f49eb19f77d44304dd703f782601c17157250e6366cd0f06b18d729bec5d770f02f512', 26, 1, 'mihalimihai@gmail.com-2020-05-06 13:41:28', '[]', 0, '2020-05-06 10:41:28', '2020-05-06 10:41:28', '2021-05-06 13:41:28'),
('10dbdd6e94669e9dc98d159023a1fb489655b0dd73a2b09c9713fdecfb9541352c037f3a9c78f784', 32, 1, 'andrei@gmail.com-2020-05-06 13:39:28', '[]', 0, '2020-05-06 10:39:28', '2020-05-06 10:39:28', '2021-05-06 13:39:28'),
('119a885226a0cfd122371aadb8a1e9523b1fd28625889b308bfff67a55adb8760e3b7481769ea8fb', 32, 1, 'andrei@gmail.com-2020-05-06 13:24:44', '[]', 0, '2020-05-06 10:24:44', '2020-05-06 10:24:44', '2021-05-06 13:24:44'),
('1338fb16010fea66a352c6b5a8dcb3ad0732861d4c8284d797ae3023333e54bed6b5cc8fa8c7531f', 31, 1, 'pppp@sda.com-2020-05-05 18:14:59', '[]', 0, '2020-05-05 15:14:59', '2020-05-05 15:14:59', '2021-05-05 18:14:59'),
('16703b4f4b68626a3859efb7885732502096556083e6e30bb682c1961576ebf993e6a5cfa1ec4471', 26, 1, 'mihalimihai@gmail.com-2020-05-06 11:34:43', '[]', 0, '2020-05-06 08:34:43', '2020-05-06 08:34:43', '2021-05-06 11:34:43'),
('1d8aa14833a412dcf9f347d68ef9622a0030acd2580a829df75a954afd57c2d983bf03b7846508bb', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:22:13', '[]', 0, '2020-05-05 12:22:13', '2020-05-05 12:22:13', '2021-05-05 15:22:13'),
('2314830018ea630edcb9d8e15e121a2e604296b5d848b5e53cdd56f1cfbc07e7c96fe2d1daaa3eed', 26, 1, 'mihalimihai@gmail.com-2020-05-05 16:14:18', '[]', 0, '2020-05-05 13:14:18', '2020-05-05 13:14:18', '2021-05-05 16:14:18'),
('24f8dd6d7063b5d72b169b252b6812041214c73471c40ec29baf45d77c9b804c1e17bdfa48b390fd', 21, 1, 'adasd@cas.com-2020-05-05 13:46:44', '[]', 0, '2020-05-05 10:46:44', '2020-05-05 10:46:44', '2021-05-05 13:46:44'),
('2a387c97a1b8552833b903508f136015204a335b84d548b6e6577ca4d6ae6d112b7477553d2fa360', 32, 1, 'andrei@gmail.com-2020-05-06 15:36:00', '[]', 0, '2020-05-06 12:36:00', '2020-05-06 12:36:00', '2021-05-06 15:36:00'),
('2ce53ac678365499f61b37c94a78fdf34a582c03f3909dd817d04648395a28e0ad637eeb61272878', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:33:35', '[]', 0, '2020-05-05 12:33:35', '2020-05-05 12:33:35', '2021-05-05 15:33:35'),
('2d1c6ffb672a17503ed6e85e809e6ba22e8a4955eaa3ece4ab40c8cb198881d4b4a0e1db7c85da07', 26, 1, 'mihalimihai@gmail.com-2020-05-06 11:34:03', '[]', 0, '2020-05-06 08:34:04', '2020-05-06 08:34:04', '2021-05-06 11:34:04'),
('3353cc36fbd95adcfde8d650877633c660f663b7bd466f1aaeedbbf85645b5137097710447bf87d7', 26, 1, 'mihalimihai@gmail.com-2020-05-05 18:25:01', '[]', 0, '2020-05-05 15:25:01', '2020-05-05 15:25:01', '2021-05-05 18:25:01'),
('3356e2433591bc43adda34b98631718005fd0019d234319468e7ca58024c00f655fd1d375252ae8c', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:02:09', '[]', 0, '2020-05-05 12:02:09', '2020-05-05 12:02:09', '2021-05-05 15:02:09'),
('341e715b3c407b075d70f8a4a3c4003b5292719c82aff3e3b94418a8eda001c709431f49d02b15c0', 32, 1, 'andrei@gmail.com-2020-05-06 13:34:05', '[]', 0, '2020-05-06 10:34:05', '2020-05-06 10:34:05', '2021-05-06 13:34:05'),
('3bd6b7ea92cd4e88db3426dc6b1949fbfd9e9dea41d2e207ab4f84942a5a1abd7ccd860d35b6f56c', 33, 1, 'mihali1@gmail.com-2020-05-06 17:07:54', '[]', 0, '2020-05-06 14:07:54', '2020-05-06 14:07:54', '2021-05-06 17:07:54'),
('3d153b1811de02d4b2e48c488604e775fc7b4f4f0fe881b675693c2e9906734627ee8f13311af7f2', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:38:20', '[]', 0, '2020-05-05 11:38:20', '2020-05-05 11:38:20', '2021-05-05 14:38:20'),
('4558d34b66270ed723a3c6782f2c033c14f6f83ddd0923825f8b00ce4a0e7b0cb39252d04fd41688', 25, 1, 'adina@gmail.com-2020-05-05 14:05:26', '[]', 0, '2020-05-05 11:05:26', '2020-05-05 11:05:26', '2021-05-05 14:05:26'),
('4dcbceb78e190afdeaf94d05f80b62dbb898cc98dce4bcf02b896bc79fb1e9865e05f41f11196ed5', 28, 1, 'mamatata@test.com-2020-05-05 15:56:00', '[]', 0, '2020-05-05 12:56:00', '2020-05-05 12:56:00', '2021-05-05 15:56:00'),
('4fae9efef00d04a6f4fb27556e567a244cdcb13b5541945edd9f5aba15c24981dab70918daa8ad3f', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:38:18', '[]', 0, '2020-05-05 11:38:18', '2020-05-05 11:38:18', '2021-05-05 14:38:18'),
('51cb8fefe4c6eaaef87d9183e445200f7d327b86bf8c479b356ad41b40124ebb690f700c04a34f10', 26, 1, 'mihalimihai@gmail.com-2020-05-05 16:22:02', '[]', 0, '2020-05-05 13:22:02', '2020-05-05 13:22:02', '2021-05-05 16:22:02'),
('52e44d357df829a311527d6f34c4fc7eee3428c16422ce28901acd6f35a5b3e22192cafdd14803d8', 26, 1, 'mihalimihai@gmail.com-2020-05-06 14:54:09', '[]', 0, '2020-05-06 11:54:09', '2020-05-06 11:54:09', '2021-05-06 14:54:09'),
('5760f05c5036d82e41ea36a77ac2137792bc32fa48d964d1b60a2837fb9b25b39db64ecb733ba22c', 33, 1, 'mihali@gmail.com-2020-05-06 16:50:07', '[]', 0, '2020-05-06 13:50:07', '2020-05-06 13:50:07', '2021-05-06 16:50:07'),
('5cddd2b0b8a39aa5ff420211968b8e9541cfd23ffaa655fdb43e3c126a29d9129f915df7f5374742', 26, 1, 'mihalimihai@gmail.com-2020-05-05 18:24:11', '[]', 0, '2020-05-05 15:24:11', '2020-05-05 15:24:11', '2021-05-05 18:24:11'),
('61b6779f914155402d49339ccdcecec898a4d004681fc0930670d860210a6f6e174be80f013f62cd', 33, 1, 'mihalimihai97@gmail.com-2020-05-06 17:13:02', '[]', 0, '2020-05-06 14:13:02', '2020-05-06 14:13:02', '2021-05-06 17:13:02'),
('63235ed726b8bc41cb7d29a4e7257f176ed0cb66ca8b84169089e9c9f8fa1c6ecdea37581e3f93de', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:40:33', '[]', 0, '2020-05-05 12:40:33', '2020-05-05 12:40:33', '2021-05-05 15:40:33'),
('63faccb1d9037798ba5faaec5a878e3e1e25e979bb813446c083a098a876811ffa893b4c3f204a5c', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:25:18', '[]', 0, '2020-05-05 11:25:18', '2020-05-05 11:25:18', '2021-05-05 14:25:18'),
('66e6134947ea4284dec56450ed6c013945c2f2ea64b91056823ee42cc8ff4b5485770c42e7132b49', 33, 1, 'mihalimihai97@gmail.com-2020-05-06 17:29:16', '[]', 0, '2020-05-06 14:29:17', '2020-05-06 14:29:17', '2021-05-06 17:29:17'),
('6e8788fef7d2c0f83bedd195cdad059d874205137b9a3a081c562fd9fa1c622e1f4cce13a209defe', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:32:13', '[]', 0, '2020-05-05 12:32:13', '2020-05-05 12:32:13', '2021-05-05 15:32:13'),
('7181cb705bffb38918c4ed146ce2611a68baf5ed7cc55438355bfb0c5d9fbd70b44d5064388df277', 26, 1, 'mihai@gmail.com-2020-05-06 15:37:57', '[]', 0, '2020-05-06 12:37:57', '2020-05-06 12:37:57', '2021-05-06 15:37:57'),
('71bf94965471a3d86990640c0225eb3c43c83f3c19739669d778010034863e2221e2399f2dc08aa8', 26, 1, 'mihaita@gmail.com-2020-05-06 15:40:35', '[]', 0, '2020-05-06 12:40:35', '2020-05-06 12:40:35', '2021-05-06 15:40:35'),
('73be644772a0bdf2d13bfd593b284810abe2a6b53a5fe357cd0513399d831e403e897676ccd6a180', 33, 1, 'mihali@gmail.com-2020-05-06 16:49:53', '[]', 0, '2020-05-06 13:49:54', '2020-05-06 13:49:54', '2021-05-06 16:49:54'),
('76489835c5d9bf33617f23dbcd112678468d3738b79790f959179a80ee15530df3ca327f01ae2551', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:38:19', '[]', 0, '2020-05-05 11:38:19', '2020-05-05 11:38:19', '2021-05-05 14:38:19'),
('797b4867f490c93422add242a139fa9f89de5d56441d1825b704a4f57968111868bc5df6185c6166', 30, 1, 'mmm@mm.com-2020-05-05 18:12:43', '[]', 0, '2020-05-05 15:12:43', '2020-05-05 15:12:43', '2021-05-05 18:12:43'),
('7f171b3655034c603e2bd1605008e09f06f00ba297b1a8c274f249866a8b7336cf2e162f74758e56', 23, 1, '321312341254@a.com-2020-05-05 13:47:51', '[]', 0, '2020-05-05 10:47:51', '2020-05-05 10:47:51', '2021-05-05 13:47:51'),
('7f3e0837fcc9d570f2a0fc65f27473fe59ece28fb029ad1ee477c0b6b21174ee1f37b0fe052dc393', 18, 1, 'emilut@zeu.com-2020-05-05 13:43:34', '[]', 0, '2020-05-05 10:43:35', '2020-05-05 10:43:35', '2021-05-05 13:43:35'),
('81f5f8385a3988022e2cf49e3243554e28293cacd2c91d32c894ffcfad59360f182342396df90734', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:21:42', '[]', 0, '2020-05-05 12:21:42', '2020-05-05 12:21:42', '2021-05-05 15:21:42'),
('8f15d830cadaa4fb92f151abfa76ab6fc1337f2f9fd00c3114300be52ba53627c81db4e91d33d998', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:20:19', '[]', 0, '2020-05-05 12:20:19', '2020-05-05 12:20:19', '2021-05-05 15:20:19'),
('94897d87191252d2532be33517c2c3acb157edcfd8fa6f916cff2428b06218884e32ea0ea97671b7', 29, 1, 'adi@oo.com-2020-05-05 18:11:45', '[]', 0, '2020-05-05 15:11:45', '2020-05-05 15:11:45', '2021-05-05 18:11:45'),
('95c13b0f740942762477d843b00c4bf209455a17e383cb70f4bdd6f613939052e6002297532f4d3e', 26, 1, 'mihalimihai@gmail.com-2020-05-05 18:37:58', '[]', 0, '2020-05-05 15:37:58', '2020-05-05 15:37:58', '2021-05-05 18:37:58'),
('9747e2dd6f7f99462df1009830fa86e9ee96432d3d0b752ccb5c7b08167a8b819e460731513a020c', 26, 1, 'mihalimihai@gmail.com-2020-05-05 16:06:56', '[]', 0, '2020-05-05 13:06:56', '2020-05-05 13:06:56', '2021-05-05 16:06:56'),
('986c849cf7cc1eeac140d6106c5a55f2098cf2d41cdc40a89abee4fa25717b41db41b29214e19695', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:43:44', '[]', 0, '2020-05-05 11:43:44', '2020-05-05 11:43:44', '2021-05-05 14:43:44'),
('98ee7487850af8a9087a05f0f159ab135cfc273c60abe76495f14a249785a615a653ab53d97deb10', 27, 1, 'aitculesei@zeu.com-2020-05-05 15:38:40', '[]', 0, '2020-05-05 12:38:40', '2020-05-05 12:38:40', '2021-05-05 15:38:40'),
('9c09e5fdd015299714a220e4e3ff6d876b9ad41f957c890a601f9c5aad40b8932aabc486084094e3', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:38:19', '[]', 0, '2020-05-05 11:38:19', '2020-05-05 11:38:19', '2021-05-05 14:38:19'),
('9f611ab1873cbd211b47215f0c3accd63254488f6fbb5115d854f45de0126000f01f6a4fe6debf29', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:37:05', '[]', 0, '2020-05-05 12:37:05', '2020-05-05 12:37:05', '2021-05-05 15:37:05'),
('a26a031948f0811bc9d094250cc96dfa4962b7beb2b4a0f0e023eda8c5da6da6c7c57917584103a7', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:24:40', '[]', 0, '2020-05-05 11:24:40', '2020-05-05 11:24:40', '2021-05-05 14:24:40'),
('ab38efb568bcf92831edb79a2a84af096e51388037fbcc16bec4ac1e6d6867a0e5f8312cea404fb3', 32, 1, 'andrei@gmail.com-2020-05-06 13:33:20', '[]', 0, '2020-05-06 10:33:20', '2020-05-06 10:33:20', '2021-05-06 13:33:20'),
('adc65567d8ff346fabb99b8f3d6faa91f99ffe2b915b756ce79ca97628d6f7f828b083cb49747d12', 26, 1, 'mihalimihai@gmail.com-2020-05-05 18:23:25', '[]', 0, '2020-05-05 15:23:25', '2020-05-05 15:23:25', '2021-05-05 18:23:25'),
('b94a94ca821b6da6d12335ec8fe0548f0d72caedac577befe84f99e8601e51aef6b6374c48c5b6ac', 26, 1, 'mihalimihai@gmail.com-2020-05-05 18:31:00', '[]', 0, '2020-05-05 15:31:00', '2020-05-05 15:31:00', '2021-05-05 18:31:00'),
('bac2feb821118ab46498024ffce938c963f4fccef0582dcea9d083d636f5d824de1d1c5b5260b386', 26, 1, 'mihalimihai@gmail.com-2020-05-05 16:53:04', '[]', 0, '2020-05-05 13:53:04', '2020-05-05 13:53:04', '2021-05-05 16:53:04'),
('bd7622e91d63a0bf5a20a293be505f043e37dff853c49c372eea0187255b1ec6853dae66cf1b190c', 26, 1, 'mihalimihai@gmail.com-2020-05-06 13:27:13', '[]', 0, '2020-05-06 10:27:13', '2020-05-06 10:27:13', '2021-05-06 13:27:13'),
('be480306f631a4ae78d92cdef8b99ea9d199a7c748f099594274172cdd1fcc05afefb9f7c3f0e42d', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:18:18', '[]', 0, '2020-05-05 12:18:18', '2020-05-05 12:18:18', '2021-05-05 15:18:18'),
('caf00bbf9884b050ae12a645600a648256e5eefd2452ab2750db4a8779f7c45aff81092ab5d73fdb', 27, 1, 'aitculesei@zeu.com-2020-05-05 15:39:18', '[]', 0, '2020-05-05 12:39:18', '2020-05-05 12:39:18', '2021-05-05 15:39:18'),
('d0d3879e55e6fe90f18327aaad12da247cabe0977068b0b2b0026b7e15cd4e611da7796c38cb9e92', 26, 1, 'mihalimihai@gmail.com-2020-05-05 16:14:52', '[]', 0, '2020-05-05 13:14:52', '2020-05-05 13:14:52', '2021-05-05 16:14:52'),
('e1271f6f81661723ff05a8ac5c32860a52495b8eba522c661f144d57cef3f81e972ea9f7263bacff', 26, 1, 'mihalimihai@gmail.com-2020-05-05 18:22:35', '[]', 0, '2020-05-05 15:22:36', '2020-05-05 15:22:36', '2021-05-05 18:22:36'),
('eab84d7b4b3c4fe58c5aad7a7571a1f66aeec140c77ef353d3f1ae5b81766b5de9965be0e4b1abbb', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:49:18', '[]', 0, '2020-05-05 12:49:19', '2020-05-05 12:49:19', '2021-05-05 15:49:19'),
('f2e7a551176b30e176c182a371b63906e8553a3204f8829dddd2cac6ec73ede96d8753fbafc31983', 26, 1, 'mihalimihai@gmail.com-2020-05-05 16:19:53', '[]', 0, '2020-05-05 13:19:53', '2020-05-05 13:19:53', '2021-05-05 16:19:53'),
('f608842d97dcb61781a690a93aafc86fb0d8a5a01ded8ee0ef346bb4c8f893940affae6b9481025a', 26, 1, 'mihalimihai@gmail.com-2020-05-05 15:25:34', '[]', 0, '2020-05-05 12:25:34', '2020-05-05 12:25:34', '2021-05-05 15:25:34'),
('f67486540252606db9cd7c4084fe055583a6554baabc76a5bad992d002c2d9ccf0f60930785b1ff6', 32, 1, 'andrei@gmail.com-2020-05-06 13:24:30', '[]', 0, '2020-05-06 10:24:30', '2020-05-06 10:24:30', '2021-05-06 13:24:30'),
('f8c305ed3e58dbc158ee66f060b7fa2af9fbd699fc78e150b652546c0c916535dcc9c1eea452acde', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:38:18', '[]', 0, '2020-05-05 11:38:18', '2020-05-05 11:38:18', '2021-05-05 14:38:18'),
('f9ef74feeab825a86b94d2a067ec0fa6e0bcd1f35e989f70a9173dc1705864a02bb641e787550871', 26, 1, 'mihalimihai@gmail.com-2020-05-05 14:38:09', '[]', 0, '2020-05-05 11:38:09', '2020-05-05 11:38:09', '2021-05-05 14:38:09'),
('fc1e9e69cb0d245fe99344b7d5c0b81cb960b845c0fa4dc8e77b45f68fab1acadfe616cec7e8c8ab', 28, 1, 'mamatata@test.com-2020-05-05 15:56:19', '[]', 0, '2020-05-05 12:56:19', '2020-05-05 12:56:19', '2021-05-05 15:56:19');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'T4ief2pT6l4mFIpXWHKYHrwlDGqKDCGIa6IjwY96', 'http://localhost', 1, 0, 0, '2020-05-03 09:40:29', '2020-05-03 09:40:29'),
(2, NULL, 'Laravel Password Grant Client', 'S0x2DCa0A4lQcEQuIB8JIBqWXedYo9n9MIMaO8wP', 'http://localhost', 0, 1, 0, '2020-05-03 09:40:29', '2020-05-03 09:40:29');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-03 09:40:29', '2020-05-03 09:40:29');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `domeniu` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `domeniu`) VALUES
(1, 'mircea vasile', 'mircea@gmail.com', '2020-05-04 14:01:32', '$2y$10$pSbciuis5o5f19YtLTUCI.1SPmq9QMav7LiSViDut.qATbbBQS/Ea', 'mihai', '2020-05-03 14:01:32', '2020-05-06 11:49:07', 'JAVAAAA'),
(2, 'ticu zeu', 'zeu@gmail.com', '2020-05-03 15:21:07', '$2y$10$kuZ0fRAp6z9hp04G1PgHn.bzQ.XFwWHjBOV6gE2aGtogNv.zmKDxm', 'ticu', '2020-05-03 15:21:07', '2020-05-06 12:23:52', 'JAVAAAA'),
(3, 'mircea vasile', 'gigi@gmail.com', NULL, 'admin', 'test', NULL, '2020-05-06 11:46:36', 'JAVAAAA'),
(25, NULL, NULL, NULL, NULL, NULL, '2020-05-05 11:05:26', '2020-05-06 11:39:01', NULL),
(26, 'aaaaa', NULL, NULL, '$2y$10$C9yWtflAllCYUbAZ26OsqOJHbIiX3mFApPnW3paLVgf3ZCSE93yrW', NULL, '2020-05-05 11:24:39', '2020-05-06 12:41:19', NULL),
(27, 'andrei ticu', 'aitculesei@zeu.com', NULL, '$2y$10$aamBJyCUvwbNtZukK5suBuL.QUdEZyDYdM6T/GFYkzBrUtYRwhLcu', NULL, '2020-05-05 12:38:40', '2020-05-05 12:38:40', NULL),
(28, 'mamatata', 'mamatata@test.com', NULL, '$2y$10$9ZehwHsmry6IQUcVSaSUQOuNgY8sUIga9a0kAkjdM0z4rbxz9HBwq', NULL, '2020-05-05 12:56:00', '2020-05-05 12:56:00', NULL),
(29, 'adisor', 'adi@oo.com', NULL, '$2y$10$oAypsM/bvHZ9c8BMS2XqBubUk3TeJl9iqbw.ph61CRvaxdtC40Cx6', NULL, '2020-05-05 15:11:45', '2020-05-05 15:11:45', NULL),
(30, 'mmm', 'mmm@mm.com', NULL, '$2y$10$hxgyt6RZt/4WHSGRuR5gpu8v5G5mtTJ0bnZ0QnwVEzmgfKUlDymN2', NULL, '2020-05-05 15:12:43', '2020-05-05 15:12:43', NULL),
(31, 'pppp', 'pppp@sda.com', NULL, '$2y$10$tlslB//oVy/HsOfVvIqI9uolW/54V3wgbGPrii/Ya0ov39P1/nlVW', NULL, '2020-05-05 15:14:59', '2020-05-05 15:14:59', NULL),
(32, 'andrei', 'andrei@gmail.com', NULL, '$2y$10$wJcCgC5oFCHUC1ooPu0O0OKwjyWLYspwulo7xNmOo3cdi1G7VUxSq', NULL, '2020-05-06 10:24:30', '2020-05-06 10:24:30', NULL),
(33, 'Mihai Mihali', 'mihalimihai97@gmail.com', NULL, '$2y$10$iT414DFr8/fn84stI5X1YujI/iSnIhAw4OZds4dr0XqqO36SdZS7i', NULL, '2020-05-06 13:49:53', '2020-05-06 14:29:55', 'SQL');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
