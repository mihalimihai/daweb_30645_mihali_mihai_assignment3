<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/user/{id}', 'UsersController@show');
Route::get('/user', 'UsersController@index');
Route::get('/getUserByEmail/{email}', 'UsersController@getUserByEmail');
Route::post('/login', 'UsersController@userLogin');
Route::post('/addUser','UsersController@store');
Route::post('/updateUser/{id}', 'UsersController@update');
