<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function userLogin(Request $request) {
        $request->validate([
            "email"           =>    "required|email",
            "password"        =>    "required|min:2"
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user=Auth::user();
            $token=$user->createToken($user->email.'-'.now());
            return response()->json([
                'token' => $token->accessToken,
                'user' => $user
            ]);
        } else {
            return response()->json(['error'=>"unauthorized"]);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = User::all();
        return response()->json($details, 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
        $user=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $user->save();
        $token = $user->createToken($user->email.'-'.now());

        if(!empty($email)){
            return response()->json("Email already registerd");
        }

        return response()->json(['user' => $user, 'token' => $token->accessToken]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if(empty($user)){
            return response()->json("User Not Found");
        };

        return response()->json($user);
    }

    public function getUserByEmail(Request $request, $email)
    {
        $user = User::where('email','=', $email)->first();

        return response()->json([
            /* 'user' =>*/ $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  AppExpense  $expense
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
        $details = User::findOrFail($id);
        $details->name = $request->name;
        $details->email = $request->email;
        $details->password = Hash::make($request->password);
        $details->domeniu = $request->domeniu;
        $details->save();
        return response()->json([
            'message' => 'Successfully updated'
        ]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
