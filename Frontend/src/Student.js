import React, {Component} from 'react';
import UserProfile from "react-user-profile"
import Poza from './poza.jpg';
import axios from 'axios';

class Student extends Component {

    constructor(props){
        super(props);
        this.state={
            _email:localStorage.getItem('email'),
        }

        axios.get('http://localhost:8000/api/getUserByEmail/' + this.state._email)
            .then(res=>{
               console.log(JSON.stringify(res.data[0]).split(",")[6].split(":")[1].split("\"")[1]);
                localStorage.setItem("userName", JSON.stringify(res.data[0]).split(",")[1].split(":")[1].split("\"")[1]);
                localStorage.setItem("userEmail", JSON.stringify(res.data[0]).split(",")[2].split(":")[1].split("\"")[1]);
                localStorage.setItem("userDomain", JSON.stringify(res.data[0]).split(",")[6].split(":")[1].split("\"")[1]);
            });
    }

    render() {
        let userName = localStorage.getItem('userName');
        let domain = localStorage.getItem('userDomain');
        let email = localStorage.getItem("userEmail");

            return (
                <div style={{margin: '0 auto', width: '100%'}}>

                    <button onClick={function update() {
                        window.location.href = "/updateStudent";
                    }}>
                        !Update My Profile!
                    </button>

                    <br/>
                    <br/>
                    <UserProfile photo={Poza} userName={userName} location={domain +"  "+ email}/>
                </div>
            )
    }
}
export default Student;