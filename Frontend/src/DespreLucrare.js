import React, {Component} from 'react';
import Background from './back.jpg';
import P2 from './p2.jpg';
import P3 from './p3.jpg';

let sectionStyle = {
    width: "100%",
    height: "400px",
    backgroundImage: "url(" + Background + ")"
};

function DespreLucrare(props){
        let content = {
            Romanian: {
                title: "Licenta",
                description:
                    "Arduino este o companie de hardware și software open source, comunitate de proiecte și utilizatori care " +
                    "proiectează și produce microcontrolere cu un singur tablou și kituri de microcontrolere pentru construirea " +
                    "dispozitivelor digitale. Proiectul meu de diploma, consta dintr-o macheta in forma de semi-sfera, facuta din ghips. " +
                    "In interiorul acestei machete se afla undeva la un numar de 80 de LED-uri, pe care user-ul trebuie sa apese pe un buton daca vede LED-ul. " +
                    "Toate aceste componente sunt conectate la placuta Arduino UNO, ulterior va fi inlocuita cu una MEGA pentru mai multe functionalitati, " +
                    "care la randul ei este conectata la calculator. Un program scris in Java primeste datele de la Arduino si deseneaza un Canvas " +
                    "cu rezultatul obtinut in urma examinarii pacientului, iar dupa acest desen, medicul specialist poate sa emita un diagnostic."
            },
            English: {
                title: "Licence",
                description:
                    "Arduino is an open source hardware and software company, a community of projects and users who design and produce single-panel " +
                    "microcontrollers and microcontroller kits for building digital devices. My diploma project consists of a semi-sphere model, made of gypsum. " +
                    "Inside this model is somewhere with 80 LEDs, which the user has to press a button if he sees the LED. " +
                    "All these components are connected to the Arduino UNO board, later it will be replaced with a MEGA for more functionalities, " +
                    "which in turn is connected to the computer. A program written in Java receives the data from " +
                    "Arduino and draws a Canvas with the result obtained after the examination of the patient, and after this drawing, " +
                    "the specialist doctor can issue a diagnosis."
            }
        };

       props.language ==="Romanian"
       ? (content=content.Romanian)
       : (content=content.English)
        return (
            <div className="nou">

                <section style={sectionStyle}>
                    <div>
                        <h3>{content.description}</h3>

                    </div>
                    <div>
                        <img src={P2}/>
                        <img src={P3}/>
                    </div>

                </section>

            </div>
        )
}
export default DespreLucrare;