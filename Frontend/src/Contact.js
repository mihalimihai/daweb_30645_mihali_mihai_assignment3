import React, {Component} from 'react';

function Contact(props){
    let content1 = {
        Romanian: {
            description:
                "Adresa: ors.Ludus jud.Mures Str.Zavoiului bl.24 sc.D ap.8"
        },
        English: {
            description:
                "Address: city of Ludus jud.Mures Str.Zavoiului bl.24 sc.D ap.8"
        }
    };

    let content2 = {
        Romanian: {
            description:
                "Telefon: 0741582434"
        },
        English: {
            description:
                "Phone: 0741582434"
        }
    };

    let content3 = {
        Romanian: {
            description:
                "Adresa email: mihalimihai97@gmail.com"
        },
        English: {
            description:
                "Email adress: mihalimihai@gmail.com"
        }
    };

    props.language ==="Romanian"
        ? (content1=content1.Romanian)
        : (content1=content1.English)
    props.language ==="Romanian"
        ? (content2=content2.Romanian)
        : (content2=content2.English)
    props.language ==="Romanian"
        ? (content3=content3.Romanian)
        : (content3=content3.English)

        return (
            <div className="container">
                <h3 className="m-3 d-flex justify-content-center" >{content1.description}</h3>
                <h3 className="m-3 d-flex justify-content-center" >{content2.description}</h3>
                <h3 className="m-3 d-flex justify-content-center" >{content3.description}</h3>
            </div>
        )
}
export default Contact;