import {Button, FormControl, FormGroup, FormLabel} from "react-bootstrap";
import React, {useState} from "react";
import axios from 'axios';
import {Redirect} from "react-router";

export default function UpdateStudent() {

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [domeniu, setDomeniu] = useState("");
    const userId = localStorage.getItem("userId");

    function handleUpdate(event) {
        event.preventDefault();
        axios.post("http://localhost:8000/api/updateUser/" + userId, {name:name, email:email, password:password, domeniu:domeniu})
            .then(res=>{
                console.log(res.data);
                return <Redirect to="/student" />
            });
    }

    return (
        <div className="Register">
            <form onSubmit={handleUpdate}>

                <FormGroup controlId="name" bsSize="large">
                    <FormLabel>Name</FormLabel>
                    <br/>
                    <FormControl
                        autoFocus
                        type="name"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                </FormGroup>

                <FormGroup controlId="email" bsSize="large">
                    <FormLabel>Email</FormLabel>
                    <br/>
                    <FormControl
                        autoFocus
                        type="email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </FormGroup>

                <FormGroup controlId="password" bsSize="large">
                    <FormLabel>Password</FormLabel>
                    <br/>
                    <FormControl
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type="password"
                    />
                </FormGroup>

                <FormGroup controlId="domeniu" bsSize="large">
                    <FormLabel>Domeniu</FormLabel>
                    <br/>
                    <FormControl
                        autofocus
                        value={domeniu}
                        onChange={e => setDomeniu(e.target.value)}
                    />
                </FormGroup>

                <br/>
                <Button block bsSize="large" type="submit">
                    Update
                </Button>
                <br/>
                <br/>
                <button onClick={function update() {
                    window.location.href = "/student";
                }}>
                    Back to my profile
                </button>
            </form>
        </div>
    );

}