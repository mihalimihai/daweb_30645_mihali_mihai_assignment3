import React, { useState } from "react";
import Navigation from "./Navigation";
import DespreLucrare from "./DespreLucrare";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import EpicMenu from "./EpicMenu";
import logo from "./arduino.png";
import Student from "./Student";
import Coordonator from "./Coordonator";
import Acasa from "./Acasa";
import Contact from "./Contact";
import Noutati from "./Noutati";
import Login from "./containers/Login";
import Register from "./containers/Register";
import Logout from "./containers/Logout";
import UpdateStudent from "./UpdateStudent";

function SelectLanguage() {
    const token = localStorage.getItem("token")
    let links1;
    let links2;
    if(token) {
         links1 = [
            {label: 'Acasa', link: '/acasa', active: true},
            {label: 'Noutati', link: '/noutati'},
            {label: 'Despre lucrare', link: '/despre'},
            {label: 'Student', link: '/student'},
            {label: 'Coordonator', link: '/coordonator'},
            {label: 'Contact', link: '/contact'},
             {label:'Delogare', link:'/logout'},
        ];
        links2 = [
            {label: 'Home', link: '/acasa', active: true},
            {label: 'News', link: '/noutati'},
            {label: 'About project', link: '/despre'},
            {label: 'Student', link: '/student'},
            {label: 'Coordinator', link: '/coordonator'},
            {label: 'Contact', link: '/contact'},
            {label:'Logout', link:'/logout'},
        ];
    }else{
        links1 = [
            {label: 'Autentificare', link: '/login'},
            {label: 'Inregistrare', link: '/register'},
        ];
        links2 = [
            {label: 'Login', link: '/login'},
            {label: 'Register', link: '/register'},
        ];
    }
    let links;
    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );
    language ==="Romanian"
        ? (links=links1)
        : (links=links2)

if(token){
    return(<div className="container center">
            <Router>
                <div>
                    <EpicMenu links={links} logo={logo} />
                    <Navigation
                        language={language}
                        handleSetLanguage={language => {
                            setLanguage(language);
                            storeLanguageInLocalStorage(language);
                        }}
                    />
                    <Switch>
                        <Route
                            exact
                            path='/student'
                            render={()=><Student/>}
                        />

                        <Route
                            exact
                            path='/coordonator'
                            render={()=><Coordonator/>}
                        />

                        <Route
                            exact
                            path='/acasa'
                            render={()=><Acasa language={language}/>}
                        />

                        <Route
                            exact
                            path='/contact'
                            render={()=><Contact language={language}/>}
                        />

                        <Route
                            exact
                            path='/noutati'
                            render={()=><Noutati language={language}/>}
                        />

                        <Route
                            exact
                            path='/despre'
                            render={()=><DespreLucrare language={language}/>}
                        />
                        <Route
                            exact
                            path='/logout'
                            render={()=><Logout language={language}/>}
                        />
                        <Route
                            exact
                            path='/updateStudent'
                            render={()=><UpdateStudent/>}
                        />

                    </Switch>
                </div>
            </Router>
            {/*<button onClick={function logout() {*/}
            {/*    localStorage.clear();*/}
            {/*    document.location.reload();*/}
            {/*    return <Redirect to="/" />*/}
            {/*}}>*/}
            {/*    Logout*/}
            {/*</button>*/}
        </div>
    );
}
else {
    return(<div className="container center">
            <Router>
                <div>
                    <EpicMenu links={links} logo={logo} />
                    <Navigation
                        language={language}
                        handleSetLanguage={language => {
                            setLanguage(language);
                            storeLanguageInLocalStorage(language);
                        }}
                    />
                    <Switch>
                        <Route
                            exact
                            path="/login"
                            render={()=><Login language={language}/>}
                        />
                        <Route
                            exact
                            path="/register"
                            render={()=><Register language={language}/>}
                        />
                    </Switch>
                </div>
            </Router>
        </div>
    );
}

}

function storeLanguageInLocalStorage(language) {
    localStorage.setItem("language", language);
}
export default SelectLanguage;