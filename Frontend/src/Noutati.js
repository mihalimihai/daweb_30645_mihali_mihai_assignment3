import React, {Component} from 'react';
import Background from './back.jpg';
import ReactPlayer from "react-player";

let sectionStyle = {
    width: "64%",
    height: "400px",
    backgroundImage: "url(" + Background + ")"
};

class Noutati extends Component{
    constructor(props){
        super(props);
        this.state = {
            text:"pppp"
        }
    }

    componentDidMount(){
        this._isMounted = true;
        var url = "fileNoutati.xml"
        fetch(url)
            .then((response) => response.text())
            .then((responseText) => {
                var data =
                responseText.split('div')[1].substring(1, responseText.split('div')[1].length-2)
                    this.setState({
                        text: data
                    })
                })
            .catch((error) => {
                console.log('Error fetching the feed: ', error);
            });
    }

    render() {
        let content1 = {
            Romanian: {
                description:
                    "Aceasta aplicatie este foarte utila pentru diferite categorii de persoane, de la varstnici pana la copii, de la\n" +
                    "                    corporatisti pana la gunoieri.\n" +
                    "                    Ajuta pentru detectia bolilor la ochi, si pentru testarea vederii, atat periferica cat si cea centrala.\n" +
                    "                    Este foarte usor de utilizat, iar precizia raspunsului dat de aplicatie este una mare, astfel oftalmologii pot sa\n" +
                    "                    emita un dignostic precis pentru pacientul respectiv. Urmand ca pacientul sa se poata trata corespunzator bolii pe care o are."
            },
            English: {
                description:
                    "This application is very useful for different categories of people, from the elderly to the children, from\n" +
                    "                    corporatists to the garbage dump.\n" +
                    "                    Helps for eye disease detection, and vision testing, both peripheral and central.\n" +
                    "                    It is very easy to use, and the accuracy of the response given by the application is high, so ophthalmologists can\n" +
                    "                    issue a precise diagnosis for the patient. Following that the patient can be properly treated for the illness he has."
            }
        };

        let content2 = {
            Romanian: {
                description:
                    "5 exemple de boli de ochi pe care aplicatia le poate detecta:"
            },
            English: {
                description:
                    "5 examples of eye diseases that the application can detect:"
            }
        };
        this.props.language === "Romanian"
            ? (content1 = content1.Romanian)
            : (content1 = content1.English)
        this.props.language === "Romanian"
            ? (content2 = content2.Romanian)
            : (content2 = content2.English)

        return (
            <div className="nou">
                <section style={sectionStyle}>
                    <div>
                        <h3 className="m-3 d-flex justify-content-center">{content2.description}</h3>
                        <ReactPlayer url="https://www.youtube.com/watch?v=xKScC5OHBgw" controls={true}/>
                    </div>
                </section>
            <h4>
                {this.state.text}
            </h4>
            </div>
        )
    }
}
export default Noutati;