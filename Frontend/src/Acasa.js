import React, {Component} from 'react';
import P1 from './p1.jpg';
import ReactPlayer from 'react-player';

function Acasa(props){
    let content1 ={
        Romanian: {
            description:
                "Aplicație medicală pentru depistarea bolilor oculare"
        },
        English: {
            description:
                "Medical application for the detection of eye diseases"
        }
    };
    let content2 ={
        Romanian: {
            description:
                "Prezentare video Arduino:"
        },
        English: {
            description:
                "Arduino video presentation:"
        }
    };
    props.language==="Romanian"
    ? (content1=content1.Romanian)
        :(content1=content1.English)

    props.language==="Romanian"
        ? (content2=content2.Romanian)
        :(content2=content2.English)

        return (
            <div className="container">
                <h3 className="m-3 d-flex justify-content-center" >{content1.description}</h3>
                <img src={P1} />
                <h4 className="m-3 d-flex justify-content-center" >{content2.description}</h4>
                <ReactPlayer url="https://www.youtube.com/watch?v=_ItSHuIJAJ8" controls={true}
                />
            </div>
        );
}
export default Acasa;