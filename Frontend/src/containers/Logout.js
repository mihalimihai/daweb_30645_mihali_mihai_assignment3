import {Button} from "react-bootstrap";
import React from "react";
import {Redirect} from "react-router";

export default function Logout() {

    function handleLogout(event) {

        localStorage.clear();
        document.location.reload();
        return <Redirect to="/" />
    }

    return(
        <form onSubmit={handleLogout}>
            <Button block bsSize="large"  type="submit">
                Logout
            </Button>
        </form>
    );
}