import {Button, FormControl, FormGroup, FormLabel} from "react-bootstrap";
import React, {useState} from "react";
import axios from 'axios';
import {Redirect} from "react-router";

function Register(props) {
    let content = {
        Romanian: {
            name:"Nume:",
            pass:"Parola:",
            email:"Email",
            button:"Inregistrare"
        },
        English: {
            name:"Name:",
            pass:"Password:",
            email:"Email",
            button:"Register"
        }
    };
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    function validateForm() {
        return email.length > 0 && password.length > 0;
    }

    function handleRegister(event) {
        event.preventDefault();
        axios.post('http://localhost:8000/api/addUser', {name:name,email:email,password:password})
            .then(res=>{
                console.log(res.data)
                alert("You have created a new account!")
                return <Redirect to="/" />
            })
    }

    props.language === "Romanian"
        ? (content = content.Romanian)
        : (content = content.English)

    return (
        <div className="Register">
            <form onSubmit={handleRegister}>

                <FormGroup controlId="name" bsSize="large">
                    <FormLabel>{content.name}</FormLabel>
                    <br/>
                    <FormControl
                        autoFocus
                        type="name"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                </FormGroup>

                <FormGroup controlId="email" bsSize="large">
                    <FormLabel>{content.email}</FormLabel>
                    <br/>
                    <FormControl
                        autoFocus
                        type="email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </FormGroup>

                <FormGroup controlId="password" bsSize="large">
                    <FormLabel>{content.pass}</FormLabel>
                    <br/>
                    <FormControl
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type="password"
                    />
                </FormGroup>

                <br/>
                <Button block bsSize="large" disabled={!validateForm()} type="submit">
                    {content.button}
                </Button>
            </form>
        </div>
    );
}

export default Register;
