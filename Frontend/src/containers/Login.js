import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel} from "react-bootstrap";
import "./Login.css";
import axios from "axios";
import {Redirect} from "react-router-dom";

export default function Login(props) {
    let content = {
        Romanian: {
            pass:"Parola:",
            email:"Email",
            button:"Autentificare"
        },
        English: {
            pass:"Password:",
            email:"Email",
            button:"Login"
        }
    };

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    function validateForm() {
        return email.length > 0 && password.length > 0;
    }

    function handleSubmit(event) {
        event.preventDefault();
        axios.post('http://localhost:8000/api/login', {email:email,password:password})
            .then(res=> {
                localStorage.setItem("token", res.data.token);
                localStorage.setItem('email', res.data.user.email);
                localStorage.setItem('userId', res.data.user.id);
                document.location.reload();
                return <Redirect to="/" />
            });
    }

    props.language === "Romanian"
        ? (content = content.Romanian)
        : (content = content.English)

    return (
        <div className="Login">
            <form onSubmit={handleSubmit}>
                <FormGroup controlId="email" bsSize="large">
                    <FormLabel>{content.email}</FormLabel>
                    <br/>
                    <FormControl
                        autoFocus
                        type="email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </FormGroup>
                <FormGroup controlId="password" bsSize="large">
                    <FormLabel>{content.pass}</FormLabel>
                    <br/>
                    <FormControl
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type="password"
                    />
                </FormGroup>
                <br/>
                <Button block bsSize="large" disabled={!validateForm()} type="submit">
                    {content.button}
                </Button>
            </form>
        </div>
    );
}